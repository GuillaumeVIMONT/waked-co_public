# MDPI Parser

https://www.mdpi.com/

## Call tree

![Call tree](call_tree.png)

## Docker command
```
docker build -t scraper-mdpi . && docker run --rm scraper-mdpi
```
