import os
import re
import time
import json
import yake
import random
import requests
from datetime import datetime
from geotext import GeoText
from bs4 import BeautifulSoup
import dateutil.parser as parser
from elasticsearch import Elasticsearch, helpers
from user_agent import generate_user_agent, generate_navigator

def get_doi(txt):
  x = re.search("(https://doi.org/)(.*)", txt)
  doi = x[2].split(" ")[0]
  return doi

def get_pubdate(txt):
    split = txt.split('-')
    return(parser.parse(split[len(split)-1]).isoformat()[0:10])
  
def get_abstract(txt):
    txt = txt.text
    if(txt[(len(txt)-13):(len(txt)-1)] == "Full article"):
        txt = txt[0:(len(txt)-13)]
    return(txt)

def get_authors_info(authors,affiliations):
    authors_raw = authors.find_all(name='span')
    affiliations = [BeautifulSoup(i, "html.parser").text for i in str(affiliations).split("<br/>")]
    #countries = [detect_country_authors(i) for i in(affiliations)]
    authors = []
    for i in range(1,len(authors_raw),2):
        author = {}
        author["name"] = authors_raw[i].text
        affiliations_id = authors_raw[i-1].text.replace(author["name"],"")
        p = re.compile(r'[0-9]')
        author["affiliations"] = [affiliations[int(i)-1] for i in p.findall(affiliations_id)]
        if(len(author["affiliations"])>0):
          author["country"] = json.loads(requests.post('http://api-geoloc:5000/country_detection', json = {'text':author["affiliations"][0]}).text)
          author["geo_point_2d"] = json.loads(requests.post('http://api-geoloc:5000/country_coordinates', json = {'country':author["country"]}).text)
        else:
          author["country"] = None
          author["geo_point_2d"] = None
        authors.append(author)
    return authors

def get_keywords(
    abstract,
    language = "en",
    max_ngram_size = 3,
    deduplication_thresold = 0.85,
    deduplication_algo = 'seqm',
    windowSize = 4,
    numOfKeywords = 5):
    keyword_list = []
    try :
        custom_kw_extractor = yake.KeywordExtractor(
            lan=language, 
            n=max_ngram_size, 
            dedupLim=deduplication_thresold, 
            dedupFunc=deduplication_algo, 
            windowsSize=windowSize, 
            top=numOfKeywords, 
            features=None)
        keywords = custom_kw_extractor.extract_keywords(abstract)
        
        for kw in keywords:
            keyword_list.append(kw[0])
    except:
        pass
    return(keyword_list)

def scrap_article(raw_article, journal):
    article = {}
    article['title'] = raw_article.find(name='a',class_='title-link').text
    pub = raw_article.find(class_='color-grey-dark').text
    article['publication_date'] = get_pubdate(pub)
    article['doi'] = get_doi(pub)
    article['abstract'] = raw_article.find(class_='abstract-full inline')#get_abstract()
    if article['abstract'] is None:
        article['abstract'] = raw_article.find(class_='abstract-div')
    article['abstract'] = get_abstract(article['abstract'])
    article['authors'] = get_authors_info(
        raw_article.find(name='div',class_='authors'),
        raw_article.find(name='div',class_='affiliations'))
    article['keywords'] = json.loads(requests.post('http://api-keywords:5000/keywords', json = {'text':article['abstract']}).text)
    article['source'] = "mdpi"
    article['journal'] = journal
    index_article(article)
    
def scrap_journal(journal, max_pages=10000):
    user_agent = generate_user_agent()
    for page in range(1, max_pages+1):
        time.sleep(random.randint(6,10))
        url = "https://www.mdpi.com/search?sort=pubdate&page_no="+str(page)+"&page_count=10&journal="+journal+"&view=abstract"
        url_raw = requests.get(url, headers={'User-Agent':user_agent})
        url_data = url_raw.content.decode()
        url_soup = BeautifulSoup(url_data, "html.parser")
        raw_articles = url_soup.find_all(name='div',class_='generic-item article-item')
        if(len(raw_articles) == 0):
            return(0)
        else:
            for raw_article in raw_articles:
                
                scrap_article(raw_article, journal)
        #print(str(datetime.today())[0:19]+", MDPI scraper, journal: "+journal+", page "+str(page)+", total articles for MDPI now: "+str(get_articles_count("covid19_api_mdpi*")))
                
def get_articles_count(index):
    es = Elasticsearch(
        [os.environ['ELASTIC_URL']],
        http_auth=(os.environ['ELASTIC_LOGIN'], os.environ['ELASTIC_PASSWORD']), 
        scheme="https", 
        port=443,
        timeout=1000000)
    return(es.cat.count(index).split()[2])

def index_article(article):
    es = Elasticsearch(
        [os.environ['ELASTIC_URL']],
        http_auth=(os.environ['ELASTIC_LOGIN'], os.environ['ELASTIC_PASSWORD']), 
        scheme="https", 
        port=443,
        timeout=1000000)
    index_name = "covid19_api_"+article["source"]+"_"+article["publication_date"][0:4]
    es.indices.create(
        index_name,
        ignore=400)
    es.index(
        index=index_name,
        id=article["doi"],
        body=article)
    
def main():
  print(str(datetime.today())[0:19]+", Scraper MDPI start.")
  journals = [
    "viruses","vetsci","vaccines","tropicalmed",
    "toxins","toxics","scipharm","reports","pharmacy",
    "harmaceutics","pharmaceuticals","pathogens","ijerph",
    "healthcare","geriatrics","ejihpe","epigenomes","diseases","diagnostics","cancers","biomolecules",
    "biomedicines","biology","bioengineering","antibodies","antibiotics"]
  random.shuffle(journals)
  for journal in journals:
    print(str(datetime.today())[0:19]+", Scraper MDPI scrapping journal "+journal)
    try:                   
      scrap_journal(journal)
    except Exception as e:
      print(str(datetime.today())[0:19]+" Scraper MDPI error: "+e)
      time.sleep(30)
  print(str(datetime.today())[0:19]+", Scraper MDPI stop.")
        
if __name__ == "__main__":
    main()

# docker build -t scraper-mdpi scraping/mdpi && docker run --network scraping_default scraper-mdpi
