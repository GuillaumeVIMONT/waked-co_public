# Wiley parser

https://novel-coronavirus.onlinelibrary.wiley.com/

## Call tree

![Call tree](call_tree.png)

## Docker command

```
docker build -t scraper-wiley . && docker run --rm scraper-wiley
```
